import requests
from bs4 import BeautifulSoup
from datetime import datetime


class Scrapper:
    """
    classdocs
    """

    test_view_data = []

    data = {
        "descriptions": None,
        "prices": None,
        "links": None,
        "date": None,
        "views": None
    }

    def __init__(self, the_url=None):
        if the_url is None:
            absolute = "http://www.impc.co.nz"
            relative = "/products/list/desktop_memory-128-page0.html"
            self.url = absolute + relative
        '''
        Constructor
        '''

    ""

    def collect_data(self):
        r = requests.get(self.url).text
        soup = BeautifulSoup(r, 'html.parser')
        table = soup.find("table", attrs={'class': 'grid productlist'})
        rows = table.find_all('tr')
        second_column = []
        third_column = []
        for row in rows:
            third_column.append(row.findAll('td')[2].contents[0])
            second_column.append(row.findAll('td')[1].contents[0])
        price_list = self.get_prices(third_column)
        description = self.get_description(second_column)
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        links = self.get_links(table)
        self.data_setter(links, description, price_list, date)

    def data_setter(self, links, description, price_list, date):
        self.data["links"] = links
        self.data["descriptions"] = description
        self.data["prices"] = price_list
        self.data["date"] = date
        page_views = self.get_page_view()
        self.data["views"] = page_views
        print(price_list)

    def get_links(self, table):
        links = []
        for link in table.findAll('a', href=True)[1::2]:
            links.append("http://www.impc.co.nz" + link['href'])
        return links

    def get_prices(self, column):
        price_list = []
        for price in column:
            dollars = int(price[1:].rsplit('.', 1)[0])
            cents = int(price[len(str(dollars)) + 2:].rsplit('+', 1)[0])
            price = []
            price.append(dollars)
            price.append(cents)
            price_list.append(price)

        return price_list

    def get_description(self, column):
        description = []
        for the_description in column:
            description.append((str(the_description.string)))
        return description

    def get_data(self):
        return self.data

    def get_page_view(self):
        image_data = []
        count = 1
        print("Getting page view data...")
        num_links = len(self.data["links"])
        for link in self.data["links"]:
            print("Working on page " +
                  str(count) + " of " + str(num_links) + "...")
            r = requests.get(link).text
            soup = BeautifulSoup(r, 'html.parser')
            image_data.append(str(
                soup.find(
                    "img", attrs={'class': 'imagenumber'})))
            count += 1
        print("Finished.. Collected " +
              str(len(image_data)) + " of " + str(num_links))
        page_view_data = []

        for datum in image_data:
            page_view_data.append(int(datum[10:].rsplit('\" class')[0]))
        self.test_view_data = page_view_data
        return page_view_data
