"""
Test the stuffs

>>> f = FileHandler()
>>> f.set_path("C:")
>>> f.get_db_path()
'C:\\\\'
"""
import pickle
import os


class FileHandler:
    """
    classdocs
    """
    path = ""
    products = []
    file_name = "database"

    def __init__(self):
        '''
        Constructor
        '''

    def set_path(self, the_path):
        if the_path[-1:] != "\\":
            the_path += "\\"
        if self.verify_path(the_path):
            self.path = the_path
        else:
            print("Invalid Path")

    def get_db_path(self):
        return self.path

    def write_database(self, the_product):
        self.products = the_product
        with open(self.path + self.file_name + '.pickle', 'wb') as handle:
            pickle.dump(self.products, handle)

    def read_database(self):
        try:
            with open(self.path + self.file_name + '.pickle', 'rb') as handle:
                loaded_products = pickle.load(handle)
                return loaded_products
        except FileNotFoundError:
            print("File not found (> >.< <) " +
                  "Try another path, or setting the file name")

    def verify_path(self, the_path):
        formatted_path = the_path.replace("\\", "/")
        return os.path.isdir(formatted_path)

    def set_file_name(self, the_file_name):
        self.file_name = the_file_name


if __name__ == "__main__":
    import doctest

    doctest.testmod()
