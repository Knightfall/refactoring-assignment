# InterpreterAssignment

# Command Help:

# get_data
#         (> ^_^ )>---[ "Dumps" the collected data ]
# do_set_db_path
#         (> ^_^ )>---[ Set the path to be used for saving the database ]
#                     [ Type the path name after the command ]
# get_db_path
#         (> ^_^ )>---[ Get the path which would be used for saving the ]
#                     [ data base ]
# save_data
#         (> ^_^ )>---[ Save data gathered from scraping, so it can be ]
#                     [ loaded in another session, loads from the ]
#                     [ default directory if a path has not been ]
#                     [ specified ]
# load_data
#         (> ^_^ )>---[ Load saved data from another session ]
# scrape_data
#         (> ^_^ )>---[ Get data from the web ]
# get_average_price
#         (> ^_^)>---[ Display the average price of the loaded products ]
# get_max_price
#         (> ^_^)>---[ Displays the highest price ]
# get_min_price
#         (> ^_^)>---[ Displays the lowest price ]
# get_min_views
#         (> ^_^)>---[ Displays the product with the least views]
# get_max_views
#         (> ^_^)>---[ Displays the product with the most views]
# get_average_views
#         (> ^_^)>---[ Displays the average views across all loaded products]
# set_file_name
#         (> ^_^)>---[ Sets name of file that gets saved when calling save_data ]
# exit
#         (> ^_^ )>---[ Exit the Program ]
