import unittest
import Controller
import HTMLParser
import CommandIntepreter as cmdI
import FileHandler
import StatisticCalculator as Calc


class UnitTester(unittest.TestCase):

    def setUp(self):
        pass

    @classmethod
    def setUpClass(cls):
        super(UnitTester, cls).setUpClass()
        cls.my_controller = Controller.Controller(
            HTMLParser.Scrapper(),
            cmdI.CommandIntepreter(),
            FileHandler.FileHandler(),
            Calc.StatisticCalculator()
        )
        cls.my_controller.scrape_data()

    def tearDown(self):
        pass

    def test_product_length(self):
        self.assertEqual(len(
            self.my_controller.products),
            20,
            "Onoes"
        )

    def test_links_length(self):
        self.assertEqual(len(
            self.my_controller.my_html_parser.data["links"]),
            20,
            "Onoes"
        )

    def test_descriptions_length(self):
        self.assertEqual(len(
            self.my_controller.my_html_parser.data
            ["descriptions"]),
            20,
            "Onoes"
        )

    def test_data_setter(self):
        self.assertEqual(len(
            self.my_controller.my_html_parser.data["links"]),
            20,
            "Error in test_data_setter"
        )

    def test_descriptions_setter(self):
        descriptions = []
        self.assertEqual(len(
            self.my_controller.descriptions_setter(descriptions)),
            20,
            "Error in test_descriptions setter"
        )

    def test_get_page_view(self):
        self.assertEqual(
            self.my_controller.my_html_parser.get_page_view(),
            self.my_controller.my_html_parser.test_view_data,
            "Error in test_get_page_view"
        )


if __name__ == "__main__":
    unittest.main()
